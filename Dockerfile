FROM alpine:latest

RUN apk add --update linux-headers libc-dev i2c-tools i2c-tools-dev make gcc g++

WORKDIR /build

COPY U6143_ssd1306 .

RUN make

ENTRYPOINT [ "./display" ]